##   ModulOSC; An open platform for OSC control of analogue devices
##
##   Copyright (C) 2014  Doug Hammond
##
##   This program is free software: you can redistribute it and/or modify
##   it under the terms of the GNU General Public License as published by
##   the Free Software Foundation, either version 3 of the License, or
##   (at your option) any later version.
##
##   This program is distributed in the hope that it will be useful,
##   but WITHOUT ANY WARRANTY; without even the implied warranty of
##   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##   GNU General Public License for more details.
##
##   You should have received a copy of the GNU General Public License
##   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#-------------------------------------------------
#
# ModulOSC : OSC/MIDI to CV Service
#
#-------------------------------------------------

QT       += core dbus network xml
QT       -= gui

TARGET = ModulOSC
CONFIG   += console
CONFIG   -= app_bundle x11

TEMPLATE = app

include(../../ModulOSC_Project.Cross.pri)

INCLUDEPATH += \
	$(PROJECT_ROOT)/QTWrap_oscpack/oscpack-read-only \
	$(PROJECT_ROOT)/QTWrap_oscpack/oscpack-read-only/osc \
	$(PROJECT_ROOT)/ModulOSC_peripherals/lib/include

SOURCES += \
	src/main.cpp \
	src/OSCServer.cpp \
	src/ModulOSC.cpp \
	src/DBus.cpp \
	src/MenuSystem.cpp \
	src/NetworkController.cpp

HEADERS += \
	src/OSCServer.h \
	src/ModulOSC.h \
	src/DBus.h \
	src/MenuSystem.h \
	src/NetworkController.h

OTHER_FILES += \
	src/com.bel.ModulOSC.xml \
	src/MenuSystem.xml

LIBS += \
	$$DESTDIR/liboscpack.so \
	$$DESTDIR/libModulOSC_peripherals.so

RESOURCES += \
	src/ModulOSC.qrc
