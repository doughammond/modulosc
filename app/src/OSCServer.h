//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <boost/shared_ptr.hpp>

#include <QObject>
#include <QtNetwork/QUdpSocket>

#include <osc/OscPacketListener.h>

/**
 * @brief The OSCServer class
 *
 * UDP socket server which reads and decodes OSC packets using oscpack
 */
class OSCServer :
	public QObject,
	public osc::OscPacketListener
{

Q_OBJECT

public:

	typedef boost::shared_ptr<OSCServer> shared_ptr;

	explicit OSCServer(const uint inPort, QObject* parent=0);

signals:

	void oscMessage(const char* path, const QVariantList args);

protected:

	virtual void ProcessMessage(const osc::ReceivedMessage &m, const IpEndpointName &remoteEndpoint);

private slots:

	void readPendingDatagrams();

private:

	QVariantList receivedMessageArgumentsToQVariantList(const osc::ReceivedMessage &m);

	QUdpSocket m_socket;

	uint m_port;
};
