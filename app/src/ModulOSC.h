//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <QCoreApplication>
#include <QThread>

#include "DBus.h"

#include "GPIO.h"
#include "I2C.h"
#include "LCDDriver.h"
#include "MCP4728.h"
#include "KL25Z.h"

#include "OSCServer.h"

#include "MenuSystem.h"

#include "NetworkController.h"

/**
 * @brief The ModulOSC service
 *
 * Instantiates IO and device drivers and internal state classes and connects them all together
 *
 */
class ModulOSC : public QCoreApplication
{
	Q_OBJECT

public:

	explicit ModulOSC(int argc, char *argv[]);

	/// BEGIN DBUS INTERFACE
public: // PROPERTIES
public Q_SLOTS: // METHODS
	void menuDown();
	void menuLeft();
	void menuRight();
	void menuUp();
	void ping();
Q_SIGNALS: // SIGNALS
	void testSig();
	/// END DBUS INTERFACE

private slots:

	void onOscMessage(const char* path, const QVariantList args);

	void routeValueToDAC(const int channel, const float value);

	void onOnlineStateChanged(const bool isOnline);

private:
	// DBus interface
	ModulOSCAdaptor m_dbusAdaptor;

	// LCD Handling
	GPIOPin::shared_ptr m_pGPIO_LCD;
	I2C::shared_ptr m_pI2C_LCD;
	LCDDriver::shared_ptr m_pLCD;

	// DAC handling
	GPIOPin::shared_ptr m_pGPIO_DAC;
	I2C::shared_ptr m_pI2C_DAC;
	MCP4728::shared_ptr m_pDAC;

	// OSC hanlding
	OSCServer::shared_ptr m_pOSCServer;

	// USB interface
	KL25Z::shared_ptr m_pKL25Z;

	// Config menu system
	MenuSystem::shared_ptr m_pMenu;

	// Network management
	NetworkController::shared_ptr m_pNetwork;
};
