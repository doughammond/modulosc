//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "ModulOSC.h"

ModulOSC::ModulOSC(int argc, char *argv[]) :
	QCoreApplication(argc, argv),

	m_dbusAdaptor(this),

#ifdef MODULOSC_LCD_ENABLE
	m_pGPIO_LCD(new GPIOPin(23, true)),
	m_pI2C_LCD(new I2C(0x20)),
	m_pLCD(new LCDDriver(m_pGPIO_LCD, m_pI2C_LCD)),
#endif

	m_pGPIO_DAC(new GPIOPin(24, false)),
	m_pI2C_DAC(new I2C(0x60)),
	m_pDAC(new MCP4728(m_pGPIO_DAC, m_pI2C_DAC)),

	m_pOSCServer(new OSCServer(8000)),

	m_pKL25Z(new KL25Z()),

	m_pMenu(new MenuSystem()),

	m_pNetwork(new NetworkController())
{
	// D-Bus setup
	QDBusConnection dbc = QDBusConnection::sessionBus();
	bool dbSuccess = false;
	dbSuccess = dbc.registerService("com.bel.ModulOSC");
	std::cout << "D-Bus RegisterService: " << dbSuccess << std::endl;
	dbSuccess = dbc.registerObject("/", this);
	std::cout << "D-Bus RegisterObject:  " << dbSuccess << std::endl;

	connect(
		m_pOSCServer.get(), SIGNAL(oscMessage(const char*,QVariantList)),
		this,				SLOT(onOscMessage(const char*,QVariantList))
	);

#ifdef MODULOSC_LCD_ENABLE
	m_pLCD->begin(16, 2);
	m_pLCD->clear();
	m_pLCD->backlight(LCDPlate::ALL_ON);

	// Menu <--> LCD connections; TODO when Menu is implemented correctly
	connect(m_pMenu.get(), SIGNAL(messageChanged(QString)), m_pLCD.get(), SLOT(message(QString)));
	connect(m_pLCD.get(), SIGNAL(btnPressDown()),  m_pMenu.get(), SLOT(nextItem()));
	connect(m_pLCD.get(), SIGNAL(btnPressUp()),    m_pMenu.get(), SLOT(prevItem()));
	connect(m_pLCD.get(), SIGNAL(btnPressRight()), m_pMenu.get(), SLOT(childItem()));
	connect(m_pLCD.get(), SIGNAL(btnPressLeft()),  m_pMenu.get(), SLOT(parentItem()));
#endif

	m_pMenu->initialise();

	connect(m_pNetwork.get(), SIGNAL(onlineStateChanged(bool)), this, SLOT(onOnlineStateChanged(bool)));
	m_pNetwork->test();
}

void ModulOSC::menuDown()
{
	m_pMenu->nextItem();
}

void ModulOSC::menuLeft()
{
	m_pMenu->parentItem();
}

void ModulOSC::menuRight()
{
	m_pMenu->childItem();
}

void ModulOSC::menuUp()
{
	m_pMenu->prevItem();
}

void ModulOSC::onOscMessage(const char *path, const QVariantList args)
{
	if (strcmp(path, "/ch1/vol") == 0) {
		routeValueToDAC(1, args[0].toFloat());
	}
	if (strcmp(path, "/ch2/vol") == 0) {
		routeValueToDAC(2, args[0].toFloat());
	}
	if (strcmp(path, "/ch3/vol") == 0) {
		routeValueToDAC(3, args[0].toFloat());
	}
	if (strcmp(path, "/ch4/vol") == 0) {
		routeValueToDAC(4, args[0].toFloat());
	}

	emit testSig();
}

void ModulOSC::routeValueToDAC(const int channel, const float value)
{
	static float valA = 0, valB = 0, valC = 0, valD = 0;

	if (channel == 1) {
		valA = value * 0xFFF;
	}
	if (channel == 2) {
		valB = value * 0xFFF;
	}
	if (channel == 3) {
		valC = value * 0xFFF;
	}
	if (channel == 4) {
		valD = value * 0xFFF;
	}

	m_pDAC->fastWrite(valA, valB, valC, valD);
}

void ModulOSC::onOnlineStateChanged(const bool isOnline) {
#ifdef MODULOSC_LCD_ENABLE
	if (isOnline) {
		m_pLCD->backlight(LCDPlate::GREEN);
	} else {
		m_pLCD->backlight(LCDPlate::RED);
	}
#endif
}

void ModulOSC::ping()
{
	std::cout << "Ping!" << std::endl;
}
