//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <boost/shared_ptr.hpp>

#include <QObject>
#include <QMap>
#include <QVariant>
#include <QtXml/QDomDocument>

#include <boost/shared_ptr.hpp>

class _Provider : public QObject
{
	Q_OBJECT

public:
	typedef boost::shared_ptr<_Provider> shared_ptr;

	explicit _Provider(QObject *parent = 0) :
		QObject(parent)
	{

	}
};


class _ProviderNetwork : public _Provider
{
	Q_OBJECT
	Q_PROPERTY(QString accessPoint READ accessPoint)
	Q_PROPERTY(QString signalStrength READ signalStrength)

public:
	typedef boost::shared_ptr<_ProviderNetwork> shared_ptr;

	explicit _ProviderNetwork(QObject *parent = 0);

	QString accessPoint() const {
		return "FooBar!";
	}

	QString signalStrength() const {
		return "-75";
	}
};

class _ProviderSettings : public _Provider
{
	Q_OBJECT
	Q_PROPERTY(QString attrA READ attrA WRITE setAttrA)
	Q_PROPERTY(QString attrB READ attrB WRITE setAttrB)

public:
	typedef boost::shared_ptr<_ProviderSettings> shared_ptr;

	explicit _ProviderSettings(QObject *parent = 0);

	QString attrA() const {
		return "Test A";
	}

	QString attrB() const {
		return "Test B";
	}

public slots:

	void setAttrA(const QString value) {

	}

	void setAttrB(const QString value) {

	}
};

typedef QMap<QString, _Provider::shared_ptr> ProviderMap;

class MenuSystem : public QObject
{
	Q_OBJECT

public:
	typedef boost::shared_ptr<MenuSystem> shared_ptr;

	explicit MenuSystem(QObject *parent = 0);

	void initialise();

signals:

	void messageChanged(const QString message);

public slots:

	void nextItem();
	void prevItem();
	void childItem();
	void parentItem();

private:

	void setCurrentItem(const QDomNode node);

private:
	ProviderMap m_providers;

	QDomDocument m_menu;
	QDomNode m_menuRoot;
	QDomNode m_currentItem;
};
