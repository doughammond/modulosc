//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "NetworkController.h"

NetworkController::NetworkController(QObject *parent) :
	QObject(parent),

	m_lastOnlineState(false)
{
	connect(&m_netConfigMgr, SIGNAL(updateCompleted()), this, SLOT(onNetUpdateCompleted()));
	connect(&m_tmrUpdate, SIGNAL(timeout()), &m_netConfigMgr, SLOT(updateConfigurations()));

	// update network states every minute
	m_tmrUpdate.start(60000);
}

void NetworkController::test() {
	QNetworkConfigurationManager::Capabilities caps = m_netConfigMgr.capabilities();
	if (caps & QNetworkConfigurationManager::CanStartAndStopInterfaces) {
		std::cout << "Net capability: CanStartAndStopInterfaces" << std::endl;
	}
	if (caps & QNetworkConfigurationManager::DirectConnectionRouting) {
		std::cout << "Net capability: DirectConnectionRouting" << std::endl;
	}
	if (caps & QNetworkConfigurationManager::SystemSessionSupport) {
		std::cout << "Net capability: SystemSessionSupport" << std::endl;
	}
	if (caps & QNetworkConfigurationManager::ApplicationLevelRoaming) {
		std::cout << "Net capability: ApplicationLevelRoaming" << std::endl;
	}
	if (caps & QNetworkConfigurationManager::ForcedRoaming) {
		std::cout << "Net capability: ForcedRoaming" << std::endl;
	}
	if (caps & QNetworkConfigurationManager::DataStatistics) {
		std::cout << "Net capability: DataStatistics" << std::endl;
	}
	if (caps & QNetworkConfigurationManager::NetworkSessionRequired) {
		std::cout << "Net capability: NetworkSessionRequired" << std::endl;
	}

	m_netConfigMgr.updateConfigurations();
}

void NetworkController::onNetUpdateCompleted() {
	bool onlineState = m_netConfigMgr.isOnline();
	if (onlineState != m_lastOnlineState) {
		m_lastOnlineState = onlineState;
		emit onlineStateChanged(onlineState);
	}

	std::cout << "Net is " << (onlineState?"":"NOT ") << "online" << std::endl;

	foreach (QNetworkConfiguration cfg, m_netConfigMgr.allConfigurations()) {
		std::cout << "Net cfg:"
		<< " purpose=" << cfg.purpose()
		<< " bearer=" << cfg.bearerType()
		<< " type=" << cfg.type()
		<< " state=" << cfg.state()
		<< " name=" << cfg.name().toStdString()
		<< std::endl;
	}
}
