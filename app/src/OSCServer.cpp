//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "OSCServer.h"

#include <ip/IpEndpointName.h>

OSCServer::OSCServer(const uint inPort, QObject *parent) :
	QObject(parent),
	m_port(inPort)
{
	m_socket.bind(QHostAddress::Any, m_port);
	connect(&m_socket, SIGNAL(readyRead()), this, SLOT(readPendingDatagrams()));
}

void OSCServer::ProcessMessage(const osc::ReceivedMessage &m, const IpEndpointName &remoteEndpoint)
{
	/*
	// Log received message : addressPattern, remoteEndpont and message args
	char addrStr[IpEndpointName::ADDRESS_AND_PORT_STRING_LENGTH];
	remoteEndpoint.AddressAndPortAsString(addrStr);

	std::cout
			<< "Received message on "
			<< m.AddressPattern()
			<< " from " << addrStr
			<< " : ";
	*/

	QVariantList args = receivedMessageArgumentsToQVariantList(m);
	/*
	foreach(QVariant arg, args)
	{
		std::cout << arg.toString().toStdString() << " ";
	}
	std::cout << std::endl;
	*/

	emit oscMessage(m.AddressPattern(), args);
}

void OSCServer::readPendingDatagrams()
{
	while (m_socket.hasPendingDatagrams())
	{
		// Read the datagram from the socket
		QByteArray datagram;
		datagram.resize(m_socket.pendingDatagramSize());
		QHostAddress senderAddr;
		quint16 senderPort;
		m_socket.readDatagram(datagram.data(), datagram.size(), &senderAddr, &senderPort);

		// Send the datagram on to oscpack for decoding
		IpEndpointName n(senderAddr.toString().toStdString().c_str(), senderPort);
		ProcessPacket(datagram.constData(), datagram.size(), n);
	}
}

QVariantList OSCServer::receivedMessageArgumentsToQVariantList(const osc::ReceivedMessage &m)
{
	QVariantList outList;

	osc::ReceivedMessage::const_iterator arg = m.ArgumentsBegin();
	while (arg != m.ArgumentsEnd()) {
		if (arg->IsBool()) {
			outList.append(QVariant(arg->AsBool()));
		}
		else if (arg->IsChar()) {
			outList.append(QVariant(arg->AsChar()));
		}
		else if (arg->IsDouble()) {
			outList.append(QVariant(arg->AsDouble()));
		}
		else if (arg->IsFloat()) {
			outList.append(QVariant(arg->AsFloat()));
		}
		else if (arg->IsInt32()) {
			outList.append(QVariant((qint32)arg->AsInt32()));
		}
		else if (arg->IsInt64()) {
			outList.append(QVariant((qint64)arg->AsInt64()));
		}
		else if (arg->IsString()) {
			outList.append(QVariant(arg->AsString()));
		}
		// TODO: map more types
		arg++;
	}

	return outList;
}
