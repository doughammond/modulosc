//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#pragma once

#include <boost/shared_ptr.hpp>

#include <QObject>
#include <QTimer>

#include <QtNetwork/QNetworkConfigurationManager>

class NetworkController : public QObject
{
	Q_OBJECT

public:

	typedef boost::shared_ptr<NetworkController> shared_ptr;

	explicit NetworkController(QObject *parent = 0);

	void test();

signals:

	void onlineStateChanged(const bool isOnline);

public slots:

	void onNetUpdateCompleted();

private:

	QNetworkConfigurationManager m_netConfigMgr;

	bool m_lastOnlineState;
	QTimer m_tmrUpdate;

};
