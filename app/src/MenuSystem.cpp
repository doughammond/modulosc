//   ModulOSC; An open platform for OSC control of analogue devices
//
//   Copyright (C) 2014  Doug Hammond
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
//   You should have received a copy of the GNU General Public License
//   along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "MenuSystem.h"

#include <QFile>

_ProviderNetwork::_ProviderNetwork(QObject *parent) :
	_Provider(parent)
{

}

_ProviderSettings::_ProviderSettings(QObject *parent) :
	_Provider(parent)
{

}

MenuSystem::MenuSystem(QObject *parent) :
	QObject(parent)
{
	m_providers.insert(
		"networking",
		_ProviderNetwork::shared_ptr(new _ProviderNetwork())
	);
	m_providers.insert(
		"settings",
		_ProviderSettings::shared_ptr(new _ProviderSettings())
	);
}

void MenuSystem::initialise()
{
	QFile configFile(":/xml/MenuSystem.xml");
	if (configFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		std::cout << "opened menu config file" << std::endl;
		if (m_menu.setContent(&configFile)) {
			std::cout << "read xml from config file" << std::endl;
		}
		configFile.close();
	}

	m_menuRoot = m_menu.documentElement();
	std::cout << "menu root is " << m_menuRoot.toElement().tagName().toStdString() << std::endl;
	setCurrentItem(m_menuRoot.firstChild());
}

void MenuSystem::nextItem()
{
	QDomNode next = m_currentItem.nextSibling();
	if (!next.isNull()) {
		setCurrentItem(next);
	} else {
		setCurrentItem(m_currentItem.parentNode().firstChild());
	}
}

void MenuSystem::prevItem()
{
	QDomNode prev = m_currentItem.previousSibling();
	if (!prev.isNull()) {
		setCurrentItem(prev);
	} else {
		setCurrentItem(m_currentItem.parentNode().lastChild());
	}

}

void MenuSystem::childItem()
{
	QString menuType = m_currentItem.toElement().attribute("type", "string");
	QString menuEdit = m_currentItem.toElement().attribute("editable", "false");

	if (menuType == "section" && m_currentItem.childNodes().size() > 0) {
		QDomNode child = m_currentItem.firstChild();
		if (!child.isNull()) {
			setCurrentItem(child);
		}
	}

	if (menuType == "string" && menuEdit == "true") { // && !m_isEditing
		// emit beginStringEdit() ?
	} // else if m_isEditing {
		// emit endStringEdit() ?
	// }
}

void MenuSystem::parentItem()
{
	QDomNode parent = m_currentItem.parentNode();

	// if m_isEditing {
		// emit cancelStringEdit()
	// } else ...

	if (parent != m_menuRoot) {
		setCurrentItem(parent);
	}
}

void MenuSystem::setCurrentItem(const QDomNode node) {
	m_currentItem = node;

	QString menuTitle = node.toElement().attribute("title", "MENU ERROR");
	QString providerName = node.toElement().attribute("provider");

	if (!m_providers.contains(providerName)) {
		std::cout << "menu item provider " << providerName.toStdString() << " not found" << std::endl;
		emit messageChanged(menuTitle + "\nMENU ERROR");
		return;
	}

	emit messageChanged(
	menuTitle +
	"\n" +
	m_providers[providerName]->property(
	node.toElement().attribute("attribute").toStdString().c_str()
	).toString()
	);
}
